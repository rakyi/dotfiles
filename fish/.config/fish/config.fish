set -gx EDITOR "emacsclient -n -c"
# fix command history search
set -g fish_color_search_match "--background=373b41"

# emacs ansi-term support
if test -n "$EMACS"
  set -x TERM eterm-color
  set fish_term256 1

  function fish_title
    true
  end
end

function e
  eval "$EDITOR" $argv
end

function o
  xdg-open $argv
end

function adu
  sudo apt update; and sudo apt full-upgrade
end

function youtube-dl
  command youtube-dl --prefer-ffmpeg $argv
end
